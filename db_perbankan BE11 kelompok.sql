-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.13-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_perbankan
CREATE DATABASE IF NOT EXISTS `db_perbankan` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `db_perbankan`;

-- Dumping structure for table db_perbankan.account
CREATE TABLE IF NOT EXISTS `account` (
  `account_number` varchar(50) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `balance` decimal(19,2) NOT NULL,
  `account_type` varchar(100) NOT NULL,
  PRIMARY KEY (`account_number`),
  KEY `FK_account_customers` (`customer_id`),
  CONSTRAINT `FK_account_customers` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table db_perbankan.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `customer_type` varchar(20) NOT NULL,
  `add_street` varchar(255) NOT NULL,
  `add_city` varchar(50) NOT NULL,
  `add_state` varchar(50) NOT NULL,
  `add_zip` varchar(5) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table db_perbankan.transaction
CREATE TABLE IF NOT EXISTS `transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` decimal(19,2) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `transaction_type` varchar(50) NOT NULL,
  `account_number` varchar(50) NOT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `FK_transaction_account` (`account_number`),
  CONSTRAINT `FK_transaction_account` FOREIGN KEY (`account_number`) REFERENCES `account` (`account_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

INSERT INTO customers VALUES
    (1,'Agus','Setiawan','2001-01-01 00:00:00','Student','Jalan Palawija','Palembang','Sumsel ',123456,987654321),
    (2,'Jalinan ','Kasih','2002-01-01 00:00:00','Employee','Jalan Bejo','Padang','Sumbar',123457,987654322),
    (3,'Pasangan','Dermawan','2003-01-01 00:00:00','Pensiunan','Jalan Merpati','Lampung','Lampung',123458,987654323),
    (4,'Ibu','Sayah','2004-01-01 00:00:00','IRT','Jalan Ayam','Jakarta','DKI Jakarta',123459,987654324),
    (5,'Jakal','Atas','2005-01-01 00:00:00','Student','Jalan Gagak','Yogyakarta','DIY',123460,987654325),
    (6,'Junaidi','Bejo','2006-01-01 00:00:00','Employee','Jalan Tonggak','Padang','Sumbar',123461,987654326),
    (7,'Ki','Agus','2007-01-01 00:00:00','Student','Jalan Cantika','Makassar','Sulsel',123462,987654327),
    (8,'Pempek','Lenggang','2008-01-01 00:00:00','Student','Jalan Pasangan','Palangkaraya','Kalteng',123463,987654328);



INSERT INTO account VALUES
    (12,1,55000,'Gold'),
    (13,2,60000,'Premium'),
    (14,2,65000,'Gold'),
    (15,3,70000,'Gold'),
    (16,3,75000,'Premium');

INSERT INTO transaction VALUES
    (10001,50000,'2001-01-01 00:00:00','Debit',12),
    (10002,55000,'2002-01-01 00:00:00','Debit',12),
    (10003,60000,'2003-01-01 00:00:00','Kredit',13),
    (10004,65000,'2004-01-01 00:00:00','Kredit',14),
    (10005,70000,'2005-01-01 00:00:00','Debit',15),
    (10006,75000,'2006-01-01 00:00:00','Debit',16),
    (10007,80000,'2007-01-01 00:00:00','Kredit',13),
    (10008,85000,'2008-01-01 00:00:00','Kredit',14);

-- inner join
select * from account INNER JOIN transaction ON account.account_number=transaction.account_number

-- inner join single customer
select * from customers INNER JOIN account ON customers.customer_id = account.customer_id WHERE first_name="Agus";

-- berdaasarkan status
select * from customers WHERE customers.customer_type="Student"; 

-- berdasarkan kredit untuk 3 table
select * from transaction OUTER JOIN (account,customers) WHERE transaction_type="Kredit";

-- inner join 3 table
select first_name
from
    customers a
        OUTER join
    account b
        on a.customer_id = b.customer_id
        inner join 
    transaction c
        on b.account_number = c.account_number;